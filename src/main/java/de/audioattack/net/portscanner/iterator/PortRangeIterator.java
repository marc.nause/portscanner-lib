/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.iterator;

/**
 * Iterator which iterates over one or more {@link PortRange}s.
 */
public class PortRangeIterator implements IPortIterator {

    private final PortRange[] myPortRanges;
    private final int mySize;
    private int myPortRangeIndex;
    private int myPortIndex;

    /**
     * Constructor.
     *
     * @param thePortRanges port ranges to iterate over
     */
    public PortRangeIterator(final PortRange... thePortRanges) {

        if (thePortRanges.length == 0) {
            throw new IllegalArgumentException("Port range must not be empty.");
        }

        myPortRanges = thePortRanges;
        mySize = getSize(myPortRanges);
    }

    @Override
    public boolean hasNext() {

        return hasMorePorts() || hasMorePortRanges();
    }

    private boolean hasMorePorts() {

        return myPortRanges[myPortRangeIndex].getMyStart()
                + myPortIndex <= myPortRanges[myPortRangeIndex].getMyEnd();
    }

    private boolean hasMorePortRanges() {

        return myPortRangeIndex < myPortRanges.length - 1;
    }

    @Override
    public Integer next() {

        if (!hasMorePorts()) {
            myPortRangeIndex++;
            myPortIndex = 0;
        }

        return myPortRanges[myPortRangeIndex].getMyStart() + myPortIndex++;
    }

    private static int getSize(final PortRange... thePortRanges) {

        int size = 0;
        for (final PortRange portRange : thePortRanges) {
            size += portRange.size();
        }

        return size;
    }

    @Override
    public int size() {
        return mySize;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
