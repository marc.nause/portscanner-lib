/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.iterator;

/**
 * A range of ports.
 */
public class PortRange {

    private final int myStart;
    private final int myEnd;

    /**
     * Constructor.
     *
     * @param theStart lower bound of ports
     * @param theEnd   higher bound of ports
     */
    public PortRange(final int theStart, final int theEnd) {

        checkPorts(theStart, theEnd);

        if (theStart <= theEnd) {
            myStart = theStart;
            myEnd = theEnd;
        } else {
            myStart = theEnd;
            myEnd = theStart;
        }

    }

    /**
     * Gets start port.
     *
     * @return lower bound of ports
     */
    public int getMyStart() {
        return myStart;
    }

    /**
     * Gets end port.
     *
     * @return higher bound of ports
     */
    public int getMyEnd() {
        return myEnd;
    }

    private static void checkPorts(final int theStart, final int theEnd) {

        if (theStart < 0 || theStart > 65535) {
            throw new IllegalArgumentException("Illegal start port, must be [0, 65535]: " + theStart);
        }

        if (theEnd < 0 || theEnd > 65535) {
            throw new IllegalArgumentException("Illegal end port, must be [0, 65535]: " + theEnd);
        }
    }

    /**
     * Gets total number of ports in range.
     *
     * @return number of ports
     */
    public int size() {

        return myEnd - myStart + 1;
    }

}
