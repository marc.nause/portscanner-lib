/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.iterator;

import java.util.NoSuchElementException;

/**
 * Empty port iterator.
 */
public final class NullPortIterator implements IPortIterator {

    private static final NullPortIterator INSTANCE = new NullPortIterator();

    private NullPortIterator() {
        super();
    }

    /**
     * Gets instance of iterator.
     *
     * @return the one and only instance of the iterator
     */
    public static NullPortIterator getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Integer next() {
        throw new NoSuchElementException();
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
