/*
 * Copyright 2015 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.iterator;

import de.audioattack.net.portscanner.util.ByteUtil;
import de.audioattack.net.portscanner.util.NetUtil;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator for network addresses (IPv4 and IPv6).
 */
public class InetAddressIterator implements Iterator<InetAddress> {

    private final BigInteger myHighestAddressOfBlock;
    private final BigInteger myTotalNumberOfAddresses;
    private final int myArraySize;
    private BigInteger myNextAddress;

    /**
     * Constructor.
     *
     * @param baseAddress         has to contain address which can be resolved to IPv4 address or
     *                            IPv6 address
     * @param networkPrefixLength determines number of hosts to be scanned
     * @param scanWholeBlock      {@code true} to scan whole block which contains baseAddress,
     *                            {@code false} to start with baseAddress
     */
    public InetAddressIterator(final InetAddress baseAddress, final int networkPrefixLength,
                               final boolean scanWholeBlock) {

        if (baseAddress instanceof Inet4Address) {
            myArraySize = 4;
        } else if (baseAddress instanceof Inet6Address) {
            myArraySize = 16;
        } else {
            throw new IllegalArgumentException(
                    "Unknown InetAddress implementation: " + baseAddress.getClass().getName());
        }

        myNextAddress = new BigInteger(baseAddress.getAddress());

        final BigInteger myFirstAddressOfBlock = myNextAddress
                .and(new BigInteger(NetUtil.getNetmask(baseAddress, networkPrefixLength)));

        myHighestAddressOfBlock = myFirstAddressOfBlock
                .or(new BigInteger(ByteUtil.invert(NetUtil.getNetmask(baseAddress, networkPrefixLength))));

        myTotalNumberOfAddresses = myHighestAddressOfBlock.subtract(myFirstAddressOfBlock)
                .add(BigInteger.ONE);

        if (scanWholeBlock) {
            myNextAddress = myFirstAddressOfBlock;
        }
    }

    @Override
    public boolean hasNext() {

        return myNextAddress.compareTo(myHighestAddressOfBlock) <= 0;
    }

    @Override
    public InetAddress next() {

        InetAddress inetAddress;
        try {
            inetAddress = InetAddress
                    .getByAddress(ByteUtil.pad(myArraySize, myNextAddress.toByteArray()));
        } catch (UnknownHostException e) {
            inetAddress = null;
        }

        if (hasNext()) {
            myNextAddress = myNextAddress.add(BigInteger.ONE);
        } else {
            throw new NoSuchElementException();
        }

        return inetAddress;
    }

    /**
     * Gets approximate count of addresses which is iterated over.
     *
     * @return approximate count
     */
    public BigInteger getApproxCount() {
        return myTotalNumberOfAddresses;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
