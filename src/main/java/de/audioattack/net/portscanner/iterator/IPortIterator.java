/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.iterator;

import java.util.Iterator;

/**
 * Interface for iterator which iterates over several ports.
 */
public interface IPortIterator extends Iterator<Integer> {

    /**
     * Count of ports the iterator iterates over.
     *
     * @return port count
     */
    int size();

}
