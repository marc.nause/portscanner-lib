/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.iterator;

import de.audioattack.net.portscanner.dictionary.IPortDictionary;

/**
 * Iterates over all ports of a dictionary.
 */
public class DictionaryPortIterator extends SinglePortIterator {

    /**
     * Constructor.
     *
     * @param thePortDictionary dictionary which contains ports to iterate over.
     */
    public DictionaryPortIterator(final IPortDictionary thePortDictionary) {
        super(thePortDictionary.getAllPorts());
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
