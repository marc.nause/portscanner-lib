/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.iterator;

import java.util.Arrays;
import java.util.NoSuchElementException;

/**
 * Iterates over one or several ports.

 */
public class SinglePortIterator implements IPortIterator {

    private final int[] myPorts;
    private int myIndex;

    /**
     * Constructor.
     *
     * @param port ports to iterate over
     */
    public SinglePortIterator(final int... port) {
        myPorts = Arrays.copyOf(port, port.length);
    }

    @Override
    public boolean hasNext() {

        return myIndex < size();
    }

    @Override
    public Integer next() {

        if (!hasNext()) {
            throw new NoSuchElementException();
        }

        return myPorts[myIndex++];
    }

    @Override
    public int size() {

        return myPorts.length;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}
