/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.listener;

import java.util.UUID;

/**
 * Listener which will be informed about progress of scan job.
 */
public interface IProgressListener {

    /**
     * Called when scan job or part of scan job is finished.
     *
     * @param jobId    unique ID of scan job
     * @param progress percentage of finished jobs (from 0.0 to 1.0)
     */
    void onJobsFinishedProgress(UUID jobId, float progress);
}
