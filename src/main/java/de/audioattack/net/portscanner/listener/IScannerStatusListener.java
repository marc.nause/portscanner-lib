/*
 * Copyright 2013 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.listener;

import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

import java.util.UUID;

/**
 * Listener which will be informed about status changes of port scanner. Method
 * calls may be executed by different Threads. If values are added to
 * Collections, thread-safe variants should be chosen.
 */
public interface IScannerStatusListener {

    /**
     * Called when scan is started.
     *
     * @param jobId unique ID of scan job
     */
    void onScanStarted(UUID jobId);

    /**
     * Called when scan is finished.
     *
     * @param jobId unique ID of scan job which has finished
     */
    void onScanFinished(UUID jobId);

    /**
     * Called when scan is aborted.
     *
     * @param jobId unique ID of scan job which has been aborted
     */
    void onScanAborted(UUID jobId);

    /**
     * Called if a new host is found via ping.
     *
     * @param jobId unique ID of scan job which has found the host
     * @param host  address of host
     */
    void onHostFound(UUID jobId, String host);

    /**
     * Called if a new port is found.
     *
     * @param jobId    unique ID of scan job which has found the port
     * @param host     address of host
     * @param port     port number
     * @param result   info about success or failure to connect to port
     * @param protocol protocol used when connecting
     */
    void onPortResult(UUID jobId, String host, int port, PortResult result,
                      TransportProtocol protocol);

}
