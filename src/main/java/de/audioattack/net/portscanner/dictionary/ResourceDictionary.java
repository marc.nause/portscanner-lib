/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.dictionary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Port dictionary which fetches data from resource file.
 * <p>
 * Format: <br />
 * portnumber\tprotocol\tdescription <br />
 * ( description is optional)
 */
public class ResourceDictionary extends AbstractDictionary {

    private static final Pattern NUMBER_PATTERN = Pattern.compile("\\d+");
    private static final Pattern SPLIT_PATTERN = Pattern.compile("\\t");
    private static final String EMPTY = "";

    private final Map<Integer, List<ProtocolInfo>> myMapping = new HashMap<>();

    /**
     * Constructor.
     *
     * @param filename resource file to fetch data from
     */
    protected ResourceDictionary(final String filename) {
        super(true);

        readData(filename);
    }

    private void readData(final String filename) {

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        Thread.currentThread().getContextClassLoader().getResourceAsStream(filename),
                        StandardCharsets.ISO_8859_1))) {

            while (reader.ready()) {
                addLine(SPLIT_PATTERN.split(reader.readLine()));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to read " + filename, e);
        }
    }

    private void addLine(final String... line) {

        if (line.length > 1 && NUMBER_PATTERN.matcher(line[0]).matches() && line[1] != null
                && !line[1].isEmpty()) {
            final int port = Integer.parseInt(line[0]);

            myMapping.computeIfAbsent(port, integer -> new LinkedList<>());
            myMapping.get(port).add(new ProtocolInfo(line[1], line.length > 2 ? line[2] : EMPTY));
        }
    }

    @Override
    protected Map<Integer, List<ProtocolInfo>> getMapping() {
        return myMapping;
    }

}
