/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.dictionary.tcp;

import de.audioattack.net.portscanner.dictionary.ResourceDictionary;

/**
 * TCP ports as known to Nmap <a href="https://nmap.org/">Nmap</a>.
 */
public final class NmapTcpDictionary extends ResourceDictionary {

    /**
     * Constructor. Creating an instance of this class is expensive. You
     * may want to keep a reference to this class or wrap it into a singleton.
     */
    public NmapTcpDictionary() {
        super("nmap_tcp_ports.txt");
    }
}
