/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.dictionary;

import java.util.List;

/**
 * Interface for port dictionaries.
 */
public interface IPortDictionary {

    /**
     * Gets info for a port.
     *
     * @param port port to get info of
     * @return port info
     */
    List<ProtocolInfo> getProtocolInfo(int port);

    /**
     * Gets all ports of the dictionary.
     *
     * @return all ports
     */
    int[] getAllPorts();
}
