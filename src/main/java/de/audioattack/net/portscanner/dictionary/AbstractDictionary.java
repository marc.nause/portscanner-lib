/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.dictionary;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Base class for dictionaries which contain a port/protocol-mapping.
 */
public abstract class AbstractDictionary implements IPortDictionary {

    private final boolean mayCache;

    private int[] ports;

    /**
     * Gets the port/protocol-mapping.
     *
     * @return the mapping
     */
    protected abstract Map<Integer, List<ProtocolInfo>> getMapping();

    /**
     * Constructor.
     *
     * @param mayCache {@code true} if mapping may be cached, {@code false} if mapping
     *                 shall be fetched using {@link #getMapping()} every time
     *                 {@link #getAllPorts()} is called.
     */
    protected AbstractDictionary(final boolean mayCache) {
        this.mayCache = mayCache;
    }

    @Override
    public List<ProtocolInfo> getProtocolInfo(final int port) {

        return getMapping().get(port);
    }

    @Override
    public int[] getAllPorts() {

        int[] array = null;

        if (!mayCache || ports == null) {
            array = new int[getMapping().size()];

            int index = 0;
            for (final int port : getMapping().keySet()) {
                array[index++] = port;
            }

            if (mayCache) {
                ports = array;
            }
        }

        return mayCache ? Arrays.copyOf(ports, ports.length) : array;
    }

}
