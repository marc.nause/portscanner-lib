/*
 * Copyright 2015 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.dictionary;

/**
 * Simple POJO to keep protocol information.
 */
public class ProtocolInfo {

    private final String myDescription;
    private final String myName;

    /**
     * Constructor.
     *
     * @param theName        name of protocol
     * @param theDescription description of protocol, may be {@code null} or empty
     */
    public ProtocolInfo(final String theName, final String theDescription) {
        myDescription = theDescription;
        myName = theName;
    }

    /**
     * Gets short description of protocol.
     *
     * @return description of protocol, may be {@code null} or empty
     */
    public String getDescription() {
        return myDescription;
    }

    /**
     * Gets name of protocol.
     *
     * @return name of protocol
     */
    public String getName() {
        return myName;
    }

}
