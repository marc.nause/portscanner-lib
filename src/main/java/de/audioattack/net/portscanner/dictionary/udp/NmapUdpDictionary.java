/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.dictionary.udp;

import de.audioattack.net.portscanner.dictionary.ResourceDictionary;

/**
 * UDP ports as known to Nmap <a href="https://nmap.org/">Nmap</a>.
 */
public final class NmapUdpDictionary extends ResourceDictionary {

    /**
     * Constructor. Creating an instance of this class is expensive. You
     * may want to keep a reference to this class or wrap it into a singleton.
     */
    public NmapUdpDictionary() {
        super("nmap_udp_ports.txt");
    }
}
