/*
 * Copyright 2013 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.util;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;

/**
 * Contains utility methods for network information.
 */
public final class NetUtil {

    /**
     * Length of IPv4 netmask in byte.
     */
    private static final int IP4_NETMASK_LENGTH = 4;

    /**
     * Length of IPv6 netmask in byte.
     */
    private static final int IP6_NETMASK_LENGTH = 16;

    /**
     * Private constructor to avoid instantiation of static utility class.
     */
    private NetUtil() {
    }

    /**
     * Creates a netmask from a given prefix length which is valid for IPv4.
     * <br />
     * <br />
     * Examples:
     * <ul>
     * <li>24 🠒 255.255.255.0</li>
     * <li>12 🠒 255.255.0.0</li>
     * <li>8 🠒 255.0.0.0</li>
     * </ul>
     *
     * @param networkPrefixLength prefix length to create netmask from.
     * @return the netmask
     */
    public static byte[] createInet4Netmask(final int networkPrefixLength) {

        if (!isValidInet4NetworkPrefixLength(networkPrefixLength)) {
            throw new IllegalArgumentException(
                    "Prefix length out of bounds [0, 32]: " + networkPrefixLength);
        }

        return ByteUtil.createBitMaskLeadingBitsSet(IP4_NETMASK_LENGTH, networkPrefixLength);
    }

    /**
     * Creates a netmask from a given prefix length which is valid for IPv6.
     *
     * @param networkPrefixLength prefix length to create netmask from.
     * @return the netmask
     * @see #createInet4Netmask(int)
     */
    public static byte[] createInet6Netmask(final int networkPrefixLength) {

        if (!isValidInet6NetworkPrefixLength(networkPrefixLength)) {
            throw new IllegalArgumentException(
                    "Prefix length out of bounds [0, 128]: " + networkPrefixLength);
        }

        return ByteUtil.createBitMaskLeadingBitsSet(IP6_NETMASK_LENGTH, networkPrefixLength);
    }

    /**
     * Gets public network addresses of device code is running on.
     *
     * @return contains all public addresses which could be found
     * @throws SocketException if an i/O error occurs
     */
    public static Collection<InetAddress> getPublicAddresses() throws SocketException {

        final Collection<InetAddress> inetAddresses = new HashSet<>();

        final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            final NetworkInterface iface = interfaces.nextElement();
            final Enumeration<InetAddress> addresses = iface.getInetAddresses();

            while (addresses.hasMoreElements()) {
                final InetAddress addr = addresses.nextElement();
                if (!addr.isLoopbackAddress()) {
                    inetAddresses.add(addr);
                }
            }
        }

        return inetAddresses;
    }

    /**
     * Creates netmask for given address (required to determine if IPv4 or IPv6 is
     * used) and network prefix length.
     *
     * @param inetAddress         valid IPv4 or IPv6 address
     * @param networkPrefixLength prefix length which matches IP version of given address
     * @return the netmask
     */
    public static byte[] getNetmask(final InetAddress inetAddress, final int networkPrefixLength) {

        final byte[] netmask;
        if (inetAddress instanceof Inet4Address) {

            netmask = NetUtil.createInet4Netmask(networkPrefixLength);
        } else if (inetAddress instanceof Inet6Address) {

            netmask = NetUtil.createInet6Netmask(networkPrefixLength);
        } else {

            throw new IllegalArgumentException(
                    "Unknown InetAddress implementation: " + inetAddress.getClass().getName());
        }

        return netmask;
    }

    /**
     * Tells if a network prefix length is valid for IP version of a given
     * InetAddress.
     *
     * @param inetAddress         valid IPv4 or IPv6 address
     * @param networkPrefixLength prefix length to check
     * @return {@code true} if network prefix length is valid, else {@code false}
     */
    public static boolean isValidNetmaskPrefixLength(final InetAddress inetAddress,
                                                     final int networkPrefixLength) {

        return (inetAddress instanceof Inet4Address
                && isValidInet4NetworkPrefixLength(networkPrefixLength))
                || (inetAddress instanceof Inet6Address
                && isValidInet6NetworkPrefixLength(networkPrefixLength));
    }

    /**
     * Tells if a network prefix length is valid for IPv4.
     *
     * @param networkPrefixLength prefix length to check
     * @return {@code true} if network prefix length is valid for IPv4, else
     * {@code false}
     */
    public static boolean isValidInet4NetworkPrefixLength(final int networkPrefixLength) {

        return networkPrefixLength >= 0 && networkPrefixLength <= 32;
    }

    /**
     * Tells if a network prefix length is valid for IPv6.
     *
     * @param networkPrefixLength prefix length to check
     * @return {@code true} if network prefix length is valid for IPv6, else
     * {@code false}
     */
    public static boolean isValidInet6NetworkPrefixLength(final int networkPrefixLength) {

        return networkPrefixLength >= 0 && networkPrefixLength <= 128;
    }

}
