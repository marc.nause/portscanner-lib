/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.scan.enums;

/**
 * Protocol to use when connecting to a port.
 */
public enum TransportProtocol {

    /**
     * Transmission Control Protocol.
     */
    TCP,
    /**
     * User Datagram Protocol.
     */
    UDP
}
