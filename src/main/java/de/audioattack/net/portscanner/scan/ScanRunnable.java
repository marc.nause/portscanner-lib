/*
 * Copyright 2015 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.scan;

import de.audioattack.net.portscanner.iterator.IPortIterator;
import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

/**
 * Scans host for reachable ports.
 */
public class ScanRunnable extends AbstractBaseRunnable {

    private final int myConnectionTimeout;
    private final InetAddress myHost;
    private final TransportProtocol myProtocol;
    private final WeakReference<IScannerStatusListener> myStatusListener;
    private final IPortIterator myPorts;
    private final boolean scanOnlyIfReachable;
    private final ScanProgress myProgress;
    private final ExecutorService myExecutor;
    private final boolean isLastHost;

    /**
     * Constructor.
     *
     * @param jobId               unique ID of job this Runnable belongs to
     * @param host                address of host to check
     * @param protocol            transport protocol for connection
     * @param pingTimeout         maximum time in ms to wait until ping times out
     * @param connectionTimeout   maximum time in ms to wait until connection times out
     * @param scanOnlyIfReachable set to {@code true} to fail fast if host can not be pinged,
     *                            {@code false} to scan anyway
     * @param ports               ports to scan
     * @param statusListener      will be informed about results, must not be {@code null}
     * @param progress            keeps track about progress of scan job
     * @param executor            executor service to run scan on
     * @param isLastHost          {@code true} if host is last host, else {@code false}
     */
    public ScanRunnable(final UUID jobId, final InetAddress host, final TransportProtocol protocol,
                        final int pingTimeout, final int connectionTimeout, final boolean scanOnlyIfReachable,
                        final IPortIterator ports, final WeakReference<IScannerStatusListener> statusListener,
                        final ScanProgress progress, final ExecutorService executor, final boolean isLastHost) {
        super(jobId, pingTimeout);
        myStatusListener = statusListener;
        this.scanOnlyIfReachable = scanOnlyIfReachable;
        myHost = host;
        myProtocol = protocol;
        myPorts = ports;
        myConnectionTimeout = connectionTimeout;
        myProgress = progress;
        myExecutor = executor;
        this.isLastHost = isLastHost;
    }

    @Override
    public void run() {

        try {

            if ((!scanOnlyIfReachable || isReachable(myHost, myStatusListener)) && myPorts != null) {

                while (myPorts.hasNext()) {

                    myExecutor.execute(
                            createScanConnector(getJobId(), new InetSocketAddress(myHost, myPorts.next()),
                                    myProtocol, myConnectionTimeout, myStatusListener));
                }

            } else {
                myProgress.incrementProgressByPortCount();
            }

            myProgress.incrementProgress();

            if (isLastHost) {
                myExecutor.shutdown();
            }

        } catch (final IOException e) {
            // skip silently
        }
    }

    private AbstractScanConnector createScanConnector(final UUID uuid,
                                                      final InetSocketAddress inetSocketAddress, final TransportProtocol protocol,
                                                      final int connectionTimeout, final WeakReference<IScannerStatusListener> statusListener) {

        if (protocol == TransportProtocol.TCP) {

            return new AbstractTcpConnector(uuid, inetSocketAddress, connectionTimeout, statusListener) {

                @Override
                public void onRequestFinished() {
                    myProgress.incrementProgress();
                }
            };

        } else if (protocol == TransportProtocol.UDP) {

            return new AbstractUdpConnector(uuid, inetSocketAddress, connectionTimeout, statusListener) {

                @Override
                public void onRequestFinished() {
                    myProgress.incrementProgress();
                }
            };

        } else {
            throw new IllegalArgumentException("Unknown transport protocol: " + protocol);
        }
    }

}
