/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.scan;

import de.audioattack.net.portscanner.listener.IProgressListener;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.UUID;

/**
 * Keeps track of scan progress.
 */
public class ScanProgress {

    private final WeakReference<IProgressListener> myProgressListenerReference;
    private final BigInteger myTotalCount;
    private final UUID myJobId;
    private final int myPortCount;
    private BigInteger finishCount = BigInteger.ZERO;

    /**
     * Constructor.
     *
     * @param jobId            ID of job to handle progress of
     * @param progressListener will be informed when progress changes
     * @param totalHostCount   number of hosts to scan
     * @param portCount        number of ports to scan
     */
    public ScanProgress(final UUID jobId, final WeakReference<IProgressListener> progressListener,
                        final BigInteger totalHostCount, final int portCount) {

        myJobId = jobId;
        myProgressListenerReference = progressListener;
        myPortCount = portCount;
        finishCount = finishCount.add(BigInteger.valueOf(portCount));
        myTotalCount = totalHostCount.multiply(BigInteger.valueOf(portCount));
    }

    /**
     * Call whenever a port has been scanned.
     */
    public synchronized void incrementProgress() {

        if (myProgressListenerReference != null) {
            finishCount = finishCount.add(BigInteger.ONE);

            final IProgressListener listener = myProgressListenerReference.get();
            if (listener != null) {
                listener.onJobsFinishedProgress(myJobId, getProgress(finishCount, myTotalCount));
            }
        }
    }

    /**
     * Adds number if ports to scan per host to progress.
     */
    public synchronized void incrementProgressByPortCount() {

        if (myProgressListenerReference != null) {
            finishCount = finishCount.add(BigInteger.valueOf(myPortCount));

            final IProgressListener listener = myProgressListenerReference.get();
            if (listener != null) {
                listener.onJobsFinishedProgress(myJobId, getProgress(finishCount, myTotalCount));
            }
        }
    }

    private static float getProgress(final BigInteger count, final BigInteger total) {

        return Math.min(1f,
                new BigDecimal(count).divide(new BigDecimal(total), 16, RoundingMode.HALF_UP).floatValue());
    }

}
