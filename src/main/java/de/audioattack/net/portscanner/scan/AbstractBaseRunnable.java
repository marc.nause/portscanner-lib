/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.scan;

import de.audioattack.net.portscanner.listener.IScannerStatusListener;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.util.UUID;

/**
 * Base runnable for tasks which check network resources.
 */
abstract class AbstractBaseRunnable implements Runnable {

    private final int myPingTimeout;

    /**
     * Unique ID of scan job this Runnable belongs to.
     */
    private final UUID myJobId;

    /**
     * Constructor.
     *
     * @param jobId       unique ID of job this Runnable belongs to
     * @param pingTimeout maximum time in ms to wait until ping times out
     */
    protected AbstractBaseRunnable(final UUID jobId, final int pingTimeout) {
        super();
        myJobId = jobId;
        myPingTimeout = pingTimeout;
    }

    /**
     * Checks if host is reachable (pingable) and informs listener if it is
     * reachable.
     *
     * @param host                    address of host to check
     * @param statusListenerReference will be informed about result, may be {@code null}
     * @return {@code true} if host is reachable, else {@code false}
     * @throws IOException if network error occurs
     */
    protected boolean isReachable(final InetAddress host,
                                  final WeakReference<IScannerStatusListener> statusListenerReference) throws IOException {
        final boolean isReachable = host.isReachable(myPingTimeout);

        if (isReachable) {
            final IScannerStatusListener listener = statusListenerReference.get();
            if (listener != null) {
                listener.onHostFound(myJobId, host.getHostAddress());
            }
        }

        return isReachable;
    }

    /**
     * Gets ID of job this Runnable belongs to.
     *
     * @return the job ID
     */
    protected UUID getJobId() {
        return myJobId;
    }

}
