/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.scan;

import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

import java.lang.ref.WeakReference;
import java.net.InetSocketAddress;
import java.util.UUID;

/**
 * Task which tries to establish a connection to a socket. If a connection can
 * be established, the data will be added to a map.
 */
abstract class AbstractScanConnector implements Runnable {

    /**
     * Address of socket to connect to.
     */
    private final InetSocketAddress myInetSocketAddress;

    /**
     * Time which process will wait before action is aborted.
     */
    private final int myTimeout;

    private final WeakReference<IScannerStatusListener> myStatusListenerReference;

    /**
     * Unique ID of scan job this Runnable belongs to.
     */
    private final UUID myJobId;

    /**
     * Constructor.
     *
     * @param jobId                   unique ID of job this Runnable belongs to
     * @param inetSocketAddress       address of socket to connect to
     * @param timeout                 time which process will wait before action is aborted
     * @param statusListenerReference listener which will be notified
     */
    AbstractScanConnector(final UUID jobId, final InetSocketAddress inetSocketAddress,
                          final int timeout, final WeakReference<IScannerStatusListener> statusListenerReference) {
        myJobId = jobId;
        this.myInetSocketAddress = inetSocketAddress;
        this.myTimeout = timeout;
        this.myStatusListenerReference = statusListenerReference;
    }

    /**
     * Gets address of host to scan.
     *
     * @return address of host to scan
     */
    protected InetSocketAddress getInetSocketAddress() {
        return myInetSocketAddress;
    }

    /**
     * Gets socket timeout.
     *
     * @return socket timeout
     */
    protected int getTimeout() {
        return myTimeout;
    }

    @Override
    public void run() {

        final PortResult result = connect();
        final IScannerStatusListener listener = myStatusListenerReference.get();
        if (listener != null) {
            listener.onPortResult(myJobId, myInetSocketAddress.getAddress().getHostAddress(),
                    myInetSocketAddress.getPort(), result, getTransportProtocol());
        }

        onRequestFinished();
    }

    /**
     * Tries to establish a connection to a socket and close it.
     *
     * @return true if establishing and closing connection did not cause any
     * errors or exceptions
     */
    protected abstract PortResult connect();

    /**
     * Gets transport protocol of connector.
     *
     * @return the protocol used by connector
     */
    protected abstract TransportProtocol getTransportProtocol();

    /**
     * Call {@link IScannerStatusListener} when request is finished.
     */
    public abstract void onRequestFinished();

}