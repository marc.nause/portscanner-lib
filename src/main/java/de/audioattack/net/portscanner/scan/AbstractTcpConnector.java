/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.scan;

import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.UUID;

/**
 * Base class for TCP connectors.
 */
abstract class AbstractTcpConnector extends AbstractScanConnector {

    /**
     * Constructor.
     *
     * @param jobId                   ID of scan job
     * @param inetSocketAddress       address of host to scan
     * @param timeout                 max. time to wait for socket connection
     * @param statusListenerReference callback to report result to
     */
    AbstractTcpConnector(final UUID jobId, final InetSocketAddress inetSocketAddress,
                         final int timeout, final WeakReference<IScannerStatusListener> statusListenerReference) {
        super(jobId, inetSocketAddress, timeout, statusListenerReference);
    }

    @Override
    protected PortResult connect() {

        PortResult ret;
        try (Socket socket = new Socket()) {
            socket.connect(getInetSocketAddress(), getTimeout());
            ret = PortResult.OPEN;
        } catch (final IOException e) {
            ret = PortResult.CLOSED;
        }
        return ret;
    }

    @Override
    protected TransportProtocol getTransportProtocol() {
        return TransportProtocol.TCP;
    }

}
