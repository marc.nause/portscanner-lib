/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.scan.enums;

/**
 * Result of a try to connect to a port.
 */
public enum PortResult {

    /**
     * Port is open.
     */
    OPEN,
    /**
     * Port seems to be opened, but access is filtered.
     */
    OPEN_FILTERED,
    /**
     * Port can't be reached.
     */
    CLOSED
}
