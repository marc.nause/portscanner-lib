/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.scan;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Creates Threads with minimum priority.
 */
public class LowPriorityThreadFactory implements ThreadFactory {

    /**
     * Constructor.
     */
    public LowPriorityThreadFactory() {
    }

    @Override
    public Thread newThread(final Runnable runnable) {
        final Thread thread = Executors.defaultThreadFactory().newThread(runnable);
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.setDaemon(true);
        return thread;
    }

}
