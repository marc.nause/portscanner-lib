/*
 * Copyright 2016 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.net.portscanner.scan;

import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.util.UUID;

/**
 * Base class for UDP connectors.
 */
abstract class AbstractUdpConnector extends AbstractScanConnector {

    private static final int MAX_TRIES = 3;

    /**
     * Constructor.
     *
     * @param jobId                   ID of scan job
     * @param inetSocketAddress       address of host to scan
     * @param timeout                 max. time to wait for socket connection
     * @param statusListenerReference callback to report result to
     */
    AbstractUdpConnector(final UUID jobId, final InetSocketAddress inetSocketAddress,
                         final int timeout, final WeakReference<IScannerStatusListener> statusListenerReference) {
        super(jobId, inetSocketAddress, timeout, statusListenerReference);
    }

    @Override
    protected PortResult connect() {

        final DatagramPacket packet = new DatagramPacket(new byte[128], 128);

        for (int tries = 0; tries < MAX_TRIES; tries++) {
            try (DatagramSocket toSocket = new DatagramSocket()) {

                toSocket.connect(getInetSocketAddress());
                toSocket.setSoTimeout(getTimeout());
                toSocket.send(packet);
                toSocket.receive(packet);

                return PortResult.OPEN;
            } catch (SocketTimeoutException e) {
                // ignore silently, we'll try again
            } catch (IOException e) {
                return PortResult.CLOSED;
            }
        }

        return PortResult.OPEN_FILTERED;
    }

    @Override
    protected TransportProtocol getTransportProtocol() {
        return TransportProtocol.UDP;
    }

}
