package de.audioattack.net.portscanner.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class ByteUtilTest {

    @Test
    void testCreateBitMaskLeadingBitsSet() {
        assertArrayEquals(new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF},
                ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 32));
        assertArrayEquals(new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFE},
                ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 31));
        assertArrayEquals(new byte[]{(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, 0x00},
                ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 24));
        assertArrayEquals(new byte[]{(byte) 0xFF, (byte) 0xFF, 0x00, 0x00},
                ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 16));
        assertArrayEquals(new byte[]{(byte) 0xFF, 0x00, 0x00, 0x00},
                ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 8));
        assertArrayEquals(new byte[]{0x00, 0x00, 0x00, 0x00},
                ByteUtil.createBitMaskLeadingBitsSet((byte) 4, (byte) 0));
    }

    @Test
    void testPad() {
        assertArrayEquals(new byte[]{0x00, 0x00, 0x01, 0x02}, ByteUtil.pad(4, new byte[]{0x01, 0x02}));
    }

}
