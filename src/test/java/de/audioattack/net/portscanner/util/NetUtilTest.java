package de.audioattack.net.portscanner.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class NetUtilTest {

    @Test
    final void testCreateInet4Netmask() {
        assertArrayEquals(new byte[]{(byte) 255, (byte) 255, (byte) 255, (byte) 255},
                NetUtil.createInet4Netmask((byte) 32));
        assertArrayEquals(new byte[]{(byte) 255, (byte) 255, (byte) 255, (byte) 0},
                NetUtil.createInet4Netmask((byte) 24));
        assertArrayEquals(new byte[]{(byte) 255, (byte) 255, (byte) 0, (byte) 0},
                NetUtil.createInet4Netmask((byte) 16));
        assertArrayEquals(new byte[]{(byte) 255, (byte) 0, (byte) 0, (byte) 0},
                NetUtil.createInet4Netmask((byte) 8));
        assertArrayEquals(new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0},
                NetUtil.createInet4Netmask((byte) 0));
    }

    @Test
    final void testCreateInet6Netmask() {
        assertArrayEquals(
                new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0,
                        (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0},
                NetUtil.createInet6Netmask((byte) 0));

        assertArrayEquals(new byte[]{(byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255,
                (byte) 255, (byte) 255, (byte) 255, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0,
                (byte) 0, (byte) 0, (byte) 0}, NetUtil.createInet6Netmask((byte) 64));
    }

}
