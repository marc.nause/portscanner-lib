package de.audioattack.net.portscanner.dictionary;

import de.audioattack.net.portscanner.dictionary.tcp.NmapTcpDictionary;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


class ResourceDictionaryTest {

    private static ResourceDictionary resourceDictionary;
    private static NmapTcpDictionary nmapDictionary;

    @BeforeAll
    static void setUp() {

        try {
            resourceDictionary = new ResourceDictionary("test.txt");
        } catch (IllegalArgumentException e) {
            fail(e.getMessage());
        }

        nmapDictionary = new NmapTcpDictionary();
    }

    @Test
    void portTest() {

        final int[] allPorts = resourceDictionary.getAllPorts();

        assertEquals(5, allPorts.length);

        Arrays.sort(allPorts);

        for (final int port : new int[]{80, 443, 12345, 665}) {
            assertTrue(Arrays.binarySearch(allPorts, port) > -1);
        }
    }

    @Test
    void protocolInfoTest() {

        assertNotEquals("anything", nmapDictionary.getProtocolInfo(80).get(0).getName());
        assertEquals("http", nmapDictionary.getProtocolInfo(80).get(0).getName());
        assertEquals("https", nmapDictionary.getProtocolInfo(443).get(0).getName());

        assertEquals("http", resourceDictionary.getProtocolInfo(80).get(0).getName());
        assertEquals("https", resourceDictionary.getProtocolInfo(443).get(0).getName());
        assertEquals("idk", resourceDictionary.getProtocolInfo(12345).get(0).getName());
        assertEquals("abc", resourceDictionary.getProtocolInfo(665).get(0).getName());
        assertEquals("example", resourceDictionary.getProtocolInfo(80).get(0).getDescription());
        assertEquals("https example", resourceDictionary.getProtocolInfo(443).get(0).getDescription());
        assertEquals("idk example", resourceDictionary.getProtocolInfo(12345).get(0).getDescription());
        assertEquals("who", resourceDictionary.getProtocolInfo(1234).get(0).getName());
        assertEquals("cares", resourceDictionary.getProtocolInfo(1234).get(1).getName());
        assertTrue(resourceDictionary.getProtocolInfo(665).get(0).getDescription().isEmpty());
        assertNull(resourceDictionary.getProtocolInfo(667));
        assertNull(resourceDictionary.getProtocolInfo(668));
    }

}
