package de.audioattack.net.portscanner.ports;

import de.audioattack.net.portscanner.iterator.PortRange;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PortRangeTest {

    @Test
    void test() {

        PortRange portRange = new PortRange(0, 0);

        assertEquals(1, portRange.size());

        portRange = new PortRange(1, 100);

        assertEquals(100, portRange.size());

    }

}
