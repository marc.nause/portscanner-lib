package de.audioattack.net.portscanner.ports;

import de.audioattack.net.portscanner.iterator.SinglePortIterator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class SinglePortIteratorTest {

    @Test
    void testSize() {

        int[] ports = new int[]{0, 1, 2};
        SinglePortIterator singlePortIterator = new SinglePortIterator(ports);

        assertEquals(ports.length, singlePortIterator.size());
    }

    @Test
    void testContent() {

        int[] ports = new int[]{0, 1, 2};
        SinglePortIterator singlePortIterator = new SinglePortIterator(ports);

        for (int i = 0; singlePortIterator.hasNext(); i++) {

            assertEquals(ports[i], singlePortIterator.next().intValue());
        }
    }

}
