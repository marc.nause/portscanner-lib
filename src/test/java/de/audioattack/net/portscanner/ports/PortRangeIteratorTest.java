package de.audioattack.net.portscanner.ports;

import de.audioattack.net.portscanner.iterator.PortRange;
import de.audioattack.net.portscanner.iterator.PortRangeIterator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;


class PortRangeIteratorTest {

    @Test
    void testPortRangeIterator() {

        PortRangeIterator genericPortIterator = new PortRangeIterator(new PortRange(0, 65535));

        int[] array1 = new int[65536];
        for (int i = 0; i <= 65535; i++) {
            array1[i] = i;
        }

        int[] array2 = new int[65536];
        for (int i = 0; genericPortIterator.hasNext(); i++) {
            array2[i] = genericPortIterator.next();
        }

        assertArrayEquals(array1, array2);

        genericPortIterator = new PortRangeIterator(new PortRange(0, 0), new PortRange(666, 667));

        array1 = new int[3];
        array1[0] = 0;
        array1[1] = 666;
        array1[2] = 667;

        array2 = new int[3];
        for (int i = 0; genericPortIterator.hasNext(); i++) {
            array2[i] = genericPortIterator.next();
        }

        assertArrayEquals(array1, array2);

    }

}
